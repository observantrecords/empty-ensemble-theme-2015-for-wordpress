<?php
/**
 * Template Name: Mobile Music Instrument
 */

/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 10:35 AM
 *
 * @package WordPress
 * @subpackage ObservantRecords
 * @since Observant Records 2015 1.0
 */

namespace ObservantRecords\WordPress\Themes\EmptyEnsemble;
?>
<?php get_header( 'mobile-music-instrument' ); ?>

<div class="col-md-8">
	<?php 	if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>


<?php get_sidebar( 'mobile-music-instrument' ); ?>
<?php get_footer( 'mobile-music-instrument' );

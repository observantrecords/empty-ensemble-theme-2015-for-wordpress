# Empty Ensemble 2015 Theme

A custom theme for [Empty Ensemble](http://emptyensemble.com/).

## Dependencies

* [Observant Records 2015 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2015-for-wordpress)

## Implementations

### Observant Records Artist Connector

The `archive-album` and `content-album` templates query the Observant Records artist database for discography data.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress)
for usage.

### Mobile Music: Push

The `mobile-music` and `mobile-music-instrument` templates provide a sidebar which may contain a widget for the Push instrument.

See the [plugin documentation](https://bitbucket.org/observantrecords/empty-ensemble-mobile-music-push)
for usage.

<?php
/**
 * Copyright (c) 2017 Observant Records.
 */

/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/15/17
 * Time: 11:29 PM
 */
?>

<?php if ( is_active_sidebar( 'sidebar-mobile-music-instrument' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar-mobile-music-instrument' ); ?>
<?php endif; ?>
